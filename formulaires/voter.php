<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2011                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
 * \***************************************************************************/

if (!defined('_ECRIRE_INC_VERSION')) {
    return;
}

include_spip('inc/actions');
include_spip('inc/editer');

function instituer_auteur_ici($auteur = array())
{
    $instituer_auteur = charger_fonction('instituer_auteur', 'inc');
    return $instituer_auteur($auteur);
}

// http://doc.spip.org/@inc_editer_mot_dist
function formulaires_voter_charger_dist($id_votation)
{

    $tab_data = interrogeAPI('votation_info', ['id_votation' => $id_votation]);
    $valeurs = [
        'titre' => $tab_data['nom']
    ];
    $id_proc = _request('id_proc');
    $valeurs['_mes_saisies'] = simplasso_saisie_vote($id_votation);
    $valeurs['id_proc'] = $id_proc;
    $valeurs = array_merge($valeurs, $tab_data);
    return $valeurs;
}


function formulaires_voter_verifier_dist($id_votation): array
{

    $mes_saisies = simplasso_saisie_vote($id_votation);
    $erreurs = saisies_verifier($mes_saisies);

    $tab_champs_api = interrogeAPI('votation_info_champs', ['id_votation' => $id_votation]);

    foreach ($tab_champs_api as $nom_groupe => $groupe) {
        foreach ($groupe['props'] as $prop) {
            $options = $prop["options"];
            $champs = 'prop' . $prop['id'];
            if (isset($options['multiple']) && $options['multiple']) {
                $tab_rep = _request($champs);
                if (isset($options['multiple_nb_min']) && $options['multiple_nb_min']) {
                    if (count($tab_rep) < $options['multiple_nb_min']) {
                        $erreurs[$champs] = 'Vous devez cocher ' . $options['multiple_nb_min'] . ' réponses minimum';
                    }
                }
                if (isset($options['multiple_nb_max']) && $options['multiple_nb_max']) {
                    if (count($tab_rep) > $options['multiple_nb_max']) {
                        $erreurs[$champs] = 'Vous devez cocher ' . $options['multiple_nb_max'] . ' réponses maximum';
                    }
                }
            }
        }
    }

    return $erreurs;
}


function formulaires_voter_traiter_dist($id_votation): array
{
    $saisies = simplasso_saisie_vote($id_votation);
    $tab_champs = [];
    foreach ($saisies as $s) {
        if (isset($s['saisies'])) {
            $tab_champs = array_merge(array_keys($s["saisies"]), $tab_champs);
        }
    }

    $args = [];
    foreach ($tab_champs as $champs) {
        $args[$champs] = _request($champs);
    }
    $id_proc = _request('id_proc');
    $args = ['id_votation' => $id_votation, 'id_proc' => $id_proc, 'reponses' => json_encode($args)];

    include_spip('inc/jsonrpc');
    $reponse = interrogeAPI('votation_voter', $args);
    if ($reponse['ok']) {
        $tab = [
            'redirect' => generer_url_public('espace_adherent', ['bloc' => 'votation']),
            'message_ok' => 'Votre vote ont bien été enregistré.'
        ];
        return $tab;
    } else {
        return array('message_erreur' => 'Erreur');
    }
}


function simplasso_saisie_vote($id_votation): array
{

    $tab_champs_api = interrogeAPI('votation_info_champs', ['id_votation' => $id_votation]);
    $tab_champs = [];
    foreach ($tab_champs_api as $nom_groupe => $groupe) {
        $s = [];
        foreach ($groupe['props'] as $prop) {
            $type = 'radio';
            $options = $prop["options"];

            if (isset($options['multiple']) && $options['multiple']) {
                $type = 'checkbox';
            }
            $s['prop' . $prop['id']] = [
                'saisie' => $type,
                'options' => [
                    'nom' => 'prop' . $prop['id'],
                    'label' => $prop['nom'],
                    'explication' => $prop['texte'],
                    'obligatoire' => 'oui',
                    'datas' => $prop['choix']
                ]
            ];
        }
        $tab_champs[$nom_groupe] = [
            'saisie' => 'fieldset',
            'options' => [
                'nom' => 'goupe' . $nom_groupe,
                'label' => $groupe["nom"] ?? '',
                'explication' => $groupe["texte"] ?? ''
            ],
            'saisies' => $s
        ];
    }

    return $tab_champs;
}
