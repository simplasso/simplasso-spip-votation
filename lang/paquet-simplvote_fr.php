<?php
if (!defined('_ECRIRE_INC_VERSION')) return;
$GLOBALS[$GLOBALS['idx_lang']] = array(
	
	'simplvote_description' => 'Plugins complémentaire à Simplasso  pour permettre aux adhérents de votre association de participer aux votes définies dans votre instance Simplasso',
	'simplvote_nom' => 'Simplasso - Votation',
	'simplvote_slogan' => 'Ajouter à votre espace adhérent un systeme de vote',
	
);

