<?php


if (!defined("_ECRIRE_INC_VERSION")) return;

function balise_LISTE_VOTATION_dist($p){
    return calculer_balise_dynamique ( $p,'LISTE_VOTATION',[]);
}

function balise_LISTE_VOTATION_stat($args,$context) {
    return [['id_entite'=>$args[0]],$context];
}

function balise_LISTE_VOTATION_dyn($args) {

    $tab = interrogeAPI('votation_liste');
    $tab =  $tab[$args['id_entite'].""]??[];
    $tab=['liste_votation'=>$tab];
    return ['inclure/adh_votation_en_cours', 0,$tab];
}



function balise_LISTE_VOTATION_HISTO_dist($p)
{
    return calculer_balise_dynamique( $p,'LISTE_VOTATION_HISTO',[]);
}

function  balise_LISTE_VOTATION_HISTO_stat ($args, $context) {
    return [['id_entite'=>$args[0]],$context];
}

function balise_LISTE_VOTATION_HISTO_dyn($args) {
    $tab = interrogeAPI('votation_close_liste');
    $tab =  $tab[$args['id_entite'].""]??[];
    $tab=['liste_votation'=>$tab];
    return ['inclure/adh_votation_histo', 0,$tab];
}


function balise_VOTATION_RESULTAT_dist($p)
{
    $id_votation = interprete_argument_balise (1, $p);
    $p->code = "interrogeAPI('votation_resultat',['id_votation'=>".$id_votation."])";
    return $p;
}